    $(function() {
		
   $.getJSON('json/browser.json', function(data) {
	   const firstFive = data.browser.slice(0, 3)	   
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#browserbest");
     });

   });
   
   $.getJSON('json/browser.json', function(data) {
	   const firstFive = data.browser.slice(3, 50)	   
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#browser");
     });

   });   
   
    $.getJSON('json/browser.json', function(data) {
	   const firstFive = data.browsersmartphone.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#browsersmartphonebest");
     });

   });

    $.getJSON('json/browser.json', function(data) {
	   const firstFive = data.browsersmartphone.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#browsersmartphone");
     });

   });
   
    $.getJSON('json/ricerca.json', function(data) {
	   const firstFive = data.ricerca.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#ricercabest");
     });

   });
   
    $.getJSON('json/ricerca.json', function(data) {
	   const firstFive = data.ricerca.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#ricerca");
     });

   });
   
    $.getJSON('json/streaming.json', function(data) {
	   const firstFive = data.streaming.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#streamingbest");
     });

   });
   
    $.getJSON('json/streaming.json', function(data) {
	   const firstFive = data.streaming.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#streaming");
     });

   }); 
   
     $.getJSON('json/streaming.json', function(data) {
	   const firstFive = data.streamingalt.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#streamingaltbest");
     });

   });
   
    $.getJSON('json/streaming.json', function(data) {
	   const firstFive = data.streamingalt.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#streamingalt");
     });

   });
   
    $.getJSON('json/traduttori.json', function(data) {
	   const firstFive = data.traduttori.slice(0, 4)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-3 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#traduttoribest");
     });

   });
   
    $.getJSON('json/traduttori.json', function(data) {
	   const firstFive = data.traduttori.slice(4, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-3 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#traduttori");
     });

   }); 
   
    $.getJSON('json/mappe.json', function(data) {
	   const firstFive = data.mappe.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#mappebest");
     });

   });
   
    $.getJSON('json/mappe.json', function(data) {
	   const firstFive = data.mappe.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#mappe");
     });

   });
   
    $.getJSON('json/chat.json', function(data) {
	   const firstFive = data.chatfed.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#chatfedbest");
     });

   });
   
    $.getJSON('json/chat.json', function(data) {
	   const firstFive = data.chatfed.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#chatfed");
     });

   });    
   
    $.getJSON('json/chat.json', function(data) {
	   const firstFive = data.chat.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#chatbest");
     });

   });
   
    $.getJSON('json/chat.json', function(data) {
	   const firstFive = data.chat.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#chat");
     });

   }); 

    $.getJSON('json/email.json', function(data) {
	   const firstFive = data.email.slice(0, 6)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#emailbest");
     });

   });
   
    $.getJSON('json/email.json', function(data) {
	   const firstFive = data.email.slice(6, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#email");
     });

   });

   $.getJSON('json/password.json', function(data) {
	   const firstFive = data.password.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#passwordbest");
     });

   });
   
   $.getJSON('json/password.json', function(data) {
	   const firstFive = data.password.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#password");
     });

   });
   
   $.getJSON('json/2fa.json', function(data) {
	   const firstFive = data.autenticazione.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#2fabest");
     });

   });
   
   $.getJSON('json/2fa.json', function(data) {
	   const firstFive = data.autenticazione.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#2fa");
     });

   });

   $.getJSON('json/note.json', function(data) {
	   const firstFive = data.note.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#notebest");
     });

   });     

   $.getJSON('json/cloud.json', function(data) {
	   const firstFive = data.cloud.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#cloudbest");
     });

   });
   
   $.getJSON('json/cloud.json', function(data) {
	   const firstFive = data.cloud.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#cloud");
     });

   });
   
   $.getJSON('json/doc.json', function(data) {
	   const firstFive = data.doc.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#docbest");
     });

   });
   
   $.getJSON('json/doc.json', function(data) {
	   const firstFive = data.doc.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#doc");
     });

   });   

   $.getJSON('json/foto.json', function(data) {
	   const firstFive = data.foto.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#fotobest");
     });

   });

   $.getJSON('json/foto.json', function(data) {
	   const firstFive = data.foto.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#foto");
     });

   });
   
   $.getJSON('json/vpn.json', function(data) {
	   const firstFive = data.vpn.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#vpnbest");
     });

   });

   $.getJSON('json/vpn.json', function(data) {
	   const firstFive = data.vpn.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#vpn");
     });

   });

   $.getJSON('json/vpn.json', function(data) {
	   const firstFive = data.vpnfree.slice(0, 4)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-3 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#vpnfreebest");
     });

   });   
   
   $.getJSON('json/adblock.json', function(data) {
	   const firstFive = data.adblock.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#adblockbest");
     });

   });

   $.getJSON('json/adblock.json', function(data) {
	   const firstFive = data.adblock.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#adblock");
     });

   });
   
   $.getJSON('json/dns.json', function(data) {
	   const firstFive = data.dns.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#dnsbest");
     });

   });

   $.getJSON('json/dns.json', function(data) {
	   const firstFive = data.dns.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#dns");
     });

   });
   
   $.getJSON('json/os.json', function(data) {
	   const firstFive = data.desktop.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#windowsbest");
     });

   });

   $.getJSON('json/os.json', function(data) {
	   const firstFive = data.desktop.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#windows");
     });

   });

   $.getJSON('json/os.json', function(data) {
	   const firstFive = data.smartphone.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#androidbest");
     });

   });

   $.getJSON('json/os.json', function(data) {
	   const firstFive = data.smartphone.slice(3, 50)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#android");
     });

   });

   $.getJSON('json/microg.json', function(data) {
	   const firstFive = data.microg.slice(0, 3)
       $.each(firstFive, function(i, f) {
          var tblRow = "<div data-aos='zoom-in' class='col-lg-4 col-md-6'><div class='col-12 stats-box'><div class='top-content'><h3 class='stats-desc'><a href='" + f.link + "'>" + f.nome + "</a></h3>" + "<img src='loghi/" + f.logo + "' width='20' height='20'></div><div class='bottom-content'><h2 class='stats-count'>" + f.e2e + "" + f.desc + "</h2></div><div class='followers-update'><br /><h5 class='up'><img src='loghi/hand-thumbs-up-fill.svg'> " + f.up + "</h5><h5 class='drop'><h5 class='drop'><img src='loghi/emoji-frown-fill.svg'> " + f.down + "</h5></div></div></div>"
           $(tblRow).appendTo("#microgbest");
     });

   }); 

});