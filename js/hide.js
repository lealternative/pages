document.getElementById("showHidebrowser").onclick = function() {
    var theDiv = document.getElementById("browser");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidebrowsersmartphone").onclick = function() {
    var theDiv = document.getElementById("browsersmartphone");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidericerca").onclick = function() {
    var theDiv = document.getElementById("ricerca");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidestreaming").onclick = function() {
    var theDiv = document.getElementById("streaming");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

//document.getElementById("showHidestreamingalt").onclick = function() {
//    var theDiv = document.getElementById("streamingalt");
//    if(theDiv.style.display == 'none') {
//        theDiv.style.display = 'flex';
//        this.innerHTML = '';
//    }
//}

//document.getElementById("showHidetraduttori").onclick = function() {
//    var theDiv = document.getElementById("traduttori");
//    if(theDiv.style.display == 'none') {
//        theDiv.style.display = 'flex';
//        this.innerHTML = '';
//    }
//}

//document.getElementById("showHidechatfed").onclick = function() {
//    var theDiv = document.getElementById("chatfed");
//    if(theDiv.style.display == 'none') {
//        theDiv.style.display = 'flex';
//        this.innerHTML = '';
//    }
//}

document.getElementById("showHidechat").onclick = function() {
    var theDiv = document.getElementById("chat");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidemappe").onclick = function() {
    var theDiv = document.getElementById("mappe");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidepassword").onclick = function() {
    var theDiv = document.getElementById("password");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHide2fa").onclick = function() {
    var theDiv = document.getElementById("2fa");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidecloud").onclick = function() {
    var theDiv = document.getElementById("cloud");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidedoc").onclick = function() {
    var theDiv = document.getElementById("doc");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidefoto").onclick = function() {
    var theDiv = document.getElementById("foto");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidevpn").onclick = function() {
    var theDiv = document.getElementById("vpn");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHidedns").onclick = function() {
    var theDiv = document.getElementById("dns");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}

document.getElementById("showHideandroid").onclick = function() {
    var theDiv = document.getElementById("android");
    if(theDiv.style.display == 'none') {
        theDiv.style.display = 'flex';
        this.innerHTML = '';
    }
}