# Lista Le Alternative

Lista Le Alternative è uno spin-off ufficiale del sito [Le Alternative](https://www.lealternative.net).

Le Alternative è un progetto personale con idee personali e dedicato alle alternative etiche ai colossi della rete. Questo spin-off vuole essere una lista di alternative semplici da trovare con un colpo d'occhio e, cosa ancora più importante, vuole essere un progetto collaborativo dove poter discutere di nuove alternative e ragionare insieme su eventuali proposte da aggiungere o rimuovere.

Per questo motivo il sito è open source, il codice è semplice HTML+CSS+JavaScript e modificando questo codice in automatico verrà modificato anche il sito.

## Il sito

Puoi trovare il sito ufficiale all'indirizzo: [lista.lealternative.net](https://lista.lealternative.net).

## Requisiti per le alternative

A differenza del sito **Le Alternative** dove vengono proposte alternative solo ed esclusivamente sulla base delle mie esperienze e delle mie idee, in questo progetto ci sono alcuni requisiti necessari perché un'alternativa possa essere inserita:

- Le Alternative **devono** essere semplici da utilizzare. Non devono essere necessarie conoscenze tecniche avanzate
    - **F-Droid** è considerata come un'alternativa valida essendo di semplicissima installazione
    - repository separati da aggiungere a F-Droid non verranno presi in considerazione per i motivi di cui sopra
- Verranno inserite solamente alternative open source.
- Non è necessario che il codice sorgente del server sia open source. Le applicazioni sono in qualche modo riproducibili e analizzabili. Nessuno può stabilire se il codice sorgente del server è lo stesso rilasciato in precedenza.
    - vengono invece presi in considerazione e in favore eventuali audit indipendenti sulla sicurezza

## Collaborare

Il codice sorgente è qui per chiunque. Prima di effettuare una *pull request* è buona norma parlarne prima aprendo una *issue*.
Se vedi un semplice errore di ortografia o di sintassi, invece, puoi ovviamente proporre una *pull request* che verrà presa in considerazione il prima possibile.

Clona il repository, fai la tua modifica e proponi la *pull request*. Se ne abbiamo discusso in precedenza verrà sicuramente accettata.

## Non ci capisco nulla di Git o di Codeberg

Nemmeno io, non preoccuparti. Se vuoi collaborare non sei obbligatǝ a utilizzare e conoscere Git o Codeberg.

**Vieni a trovarci** su [Telegram](https://t.me/LeAlternativeChat), su [Matrix](https://matrix.to/#/%23LeAlternativeGruppo:matrix.org), [Reddit](https://www.reddit.com/r/LeAlternative/) o meglio ancora [Feddit](https://feddit.it/c/lealternative). Possiamo parlarne pubblicamente anche su [Mastodon](https://mastodon.uno/@lealternative) se preferisci. Esponi le tue idee e i tuoi dubbi con la comunità, ci penserà qualcuno a creare la *issue* o a inserire direttamente la modifica online.

## Building

Il sito non è fatto con nessun builder. Puoi banalmente fare copia e incolla di questi file su qualunque server e andrà tutto senza che dobbiate avere la minima conoscenza tecnica di Linux, di *build* o di altre piattaforme. Vi basterà conoscere l'**HTML** di base. Tutto questo è fatto appositamente per poter rendere il tutto il più possibile **semplice** per chiunque.